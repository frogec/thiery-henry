import Toybox.Application;
import Toybox.Graphics;
import Toybox.Lang;
import Toybox.System;
import Toybox.WatchUi;
using Toybox.ActivityMonitor as Mon;
using Toybox.Time.Gregorian as Date;
using Toybox.SensorHistory;

class WatchFaceView extends WatchUi.WatchFace {

    function initialize() {
        WatchFace.initialize();
    }

    // Load your resources here
    function onLayout(dc as Dc) as Void {
        
        //dc.drawBitmap(100, 100, Toybox.WatchUi.loadResource(Rez.Drawables.Wizard));
        setLayout(Rez.Layouts.WatchFace(dc));
    }

    // Called when this View is brought to the foreground. Restore
    // the state of this View and prepare it to be shown. This includes
    // loading resources into memory.
    function onShow() as Void {
    }

        function stringReplace(str, oldString, newString)
    {
    var result = str;

    while (true)
    {
    var index = result.find(oldString);

    if (index != null)
    {
    var index2 = index+oldString.length();
    result = result.substring(0, index) + newString + result.substring(index2, result.length());
    }
    else
    {
    return result;
    }
    }

    return null;
    }

    // Build up the time string
    private function getTimeString() {
        var clockTime = System.getClockTime();
        var info = System.getDeviceSettings();

        var hour = clockTime.hour;

        if( !info.is24Hour ) {
            hour = clockTime.hour % 12;
            if (hour == 0) {
                hour = 12;
            }
        }

        return Lang.format("$1$:$2$", [hour, clockTime.min.format("%02d")]);
    }

    private function getDateDisplayString() {        
    	var now = Time.now();
        var date = Date.info(now, Time.FORMAT_LONG);
        
        var dateString = Lang.format("$1$ $2$ $3$", [stringReplace(date.day_of_week,"Č","C"),  date.day, date.month]);
        return dateString;
    }


    private function getBatteryString() {
    	var battery = System.getSystemStats().battery;				
	    return battery.format("%d")+"%";	
    }
    
    private function retrieveHeartrateText() {


        var heartrateIterator = ActivityMonitor.getHeartRateHistory(null, false);
	    var currentHeartrate = heartrateIterator.next().heartRate;

        if(currentHeartrate == Mon.INVALID_HR_SAMPLE) {
            return "";
        }		

            return currentHeartrate.format("%d");
        


        
    }

    // Create a method to get the SensorHistoryIterator object
    function getIterator() {
        // Check device for SensorHistory compatibility
        if ((Toybox has :SensorHistory) && (Toybox.SensorHistory has :getBodyBatteryHistory)) {
            // Set up the method with parameters
            return Toybox.SensorHistory.getBodyBatteryHistory({});
        }
        return null;
    }

    private function retrieveBodyBatteryText() {

        // get the body battery iterator object
        var bbIterator = getIterator();
        var sample = bbIterator.next();       

        return sample.data.format("%d");
        
    }

    // Update the view
    function onUpdate(dc as Dc) as Void {
        // Get the current time and format it correctly
        var timeFormat = "$1$:$2$";
        var clockTime = System.getClockTime();
        var hours = clockTime.hour;
        if (!System.getDeviceSettings().is24Hour) {
            if (hours > 12) {
                hours = hours - 12;
            }
        } else {
            if (getApp().getProperty("UseMilitaryFormat")) {
                timeFormat = "$1$$2$";
                hours = hours.format("%02d");
            }
        }
        var timeString = Lang.format(timeFormat, [hours, clockTime.min.format("%02d")]);

        // Update the view
        var view = View.findDrawableById("TimeLabel") as Text;
        view.setColor(Graphics.COLOR_WHITE);
        view.setText(timeString);

        //HR
        view = View.findDrawableById("HR") as Text;
        view.setColor(Graphics.COLOR_WHITE);
        view.setText( "HR "+retrieveHeartrateText());

        //date
        view = View.findDrawableById("Date") as Text;
        view.setColor(Graphics.COLOR_WHITE);
        view.setText(getDateDisplayString());

        //battery
        /*view = View.findDrawableById("Battery") as Text;
        view.setColor(Graphics.COLOR_WHITE);
        view.setText( getBatteryString());*/

        //body battery
        view = View.findDrawableById("BodyBattery") as Text;
        view.setColor(Graphics.COLOR_WHITE);
        view.setText("Body "+ retrieveBodyBatteryText());


        //dc.drawBitmap(100, 100, Toybox.WatchUi.loadResource(Rez.Drawables.Wizard));

        // Call the parent onUpdate function to redraw the layout
        View.onUpdate(dc);
    }

    // Called when this View is removed from the screen. Save the
    // state of this View here. This includes freeing resources from
    // memory.
    function onHide() as Void {
    }

    // The user has just looked at their watch. Timers and animations may be started here.
    function onExitSleep() as Void {
    }

    // Terminate any active timers and prepare for slow updates.
    function onEnterSleep() as Void {
    }

}
